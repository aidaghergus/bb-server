package ism.ase.ro.bb.service;


import ism.ase.ro.bb.model.entity.Token;
import ism.ase.ro.bb.model.entity.User;

import java.util.List;
import java.util.Set;

public interface UserService {

    User findById(Long id);

    void signUp(User user);

    Token authenticateWithCredentials(User user);

    Long authenticateWithToken(String token);

    List<User> findAll();

    List<User> findByFilter(String filter);


}
