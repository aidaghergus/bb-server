package ism.ase.ro.bb.service.impl;

import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.model.entity.Role;
import ism.ase.ro.bb.repository.RoleRepository;
import ism.ase.ro.bb.service.RoleService;
import ism.ase.ro.bb.utils.SuccesMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private static final Logger LOGGER = new Logger(RoleServiceImpl.class);

    @Autowired
    private RoleRepository roleJpaRepository;

    public List<Role> findAll() {
        LOGGER.info(SuccesMessage.ROLES_SENDED);
        return roleJpaRepository.findAll();
    }
}
