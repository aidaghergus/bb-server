package ism.ase.ro.bb.service.impl;

import ism.ase.ro.bb.model.entity.Item;
import ism.ase.ro.bb.model.entity.Offer;
import ism.ase.ro.bb.model.entity.User;
import ism.ase.ro.bb.repository.ItemRepository;
import ism.ase.ro.bb.service.OfferService;

import ism.ase.ro.bb.repository.OfferRepository;
import ism.ase.ro.bb.utils.ItemStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfferServiceImpl implements OfferService {

    @Autowired
    OfferRepository offerRepository;

    @Autowired
    ItemRepository itemRepository;
    @Override
    public void save(Offer offer) throws Exception {
        if(offer.getItem().getStatus()!=ItemStatus.AVAILABLE)
            throw new Exception("Item is traded or waiting to be traded");
        offerRepository.save(offer);
        Item item=offer.getItem();
        item.setStatus(ItemStatus.WAITING);
        itemRepository.save(item);
    }

    @Override
    public void delete(Offer offer) { offerRepository.delete(offer);}

    @Override
    public Offer findById(Long id) {
        return offerRepository.findById(id).isPresent()?offerRepository.findById(id).get():null;
    }

    @Override
    public List<Offer> findByUser(User user) {
        return offerRepository.findAllByUser(user);
    }

    @Override
    public List<Offer> findByItem(Item item) {
        return offerRepository.findAllByItem(item);
    }
}
