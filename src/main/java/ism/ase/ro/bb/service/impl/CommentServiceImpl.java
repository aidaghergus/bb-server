package ism.ase.ro.bb.service.impl;


import ism.ase.ro.bb.model.entity.Comment;
import ism.ase.ro.bb.model.entity.User;
import ism.ase.ro.bb.repository.CommentRepository;
import ism.ase.ro.bb.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    CommentRepository commentRepository;

    @Override
    public void save(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    public List<Comment> findByUser(User user) {
        return commentRepository.findAllByUserId(user.getId());
    }

    @Override
    public List<Comment> findByItem(Long itemId) {
        return commentRepository.findAllByItemId(itemId);
    }
}
