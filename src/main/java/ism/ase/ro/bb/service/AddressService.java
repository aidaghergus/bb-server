package ism.ase.ro.bb.service;

import ism.ase.ro.bb.model.entity.Address;
import ism.ase.ro.bb.model.entity.User;

import java.util.List;

public interface AddressService {
    void save(Address address);

    List<Address> findByUser(User user);

}
