package ism.ase.ro.bb.service.impl;

import ism.ase.ro.bb.errorhandling.error.ErrorMessage;
import ism.ase.ro.bb.errorhandling.exception.user.*;
import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.model.entity.Token;
import ism.ase.ro.bb.model.entity.User;
import ism.ase.ro.bb.repository.RoleRepository;
import ism.ase.ro.bb.repository.UserRepository;
import ism.ase.ro.bb.security.Authenticator;
import ism.ase.ro.bb.security.PasswordCrypter;
import ism.ase.ro.bb.service.UserService;
import ism.ase.ro.bb.utils.SuccesMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = new Logger(UserServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    Authenticator authenticator;

    @Autowired
    PasswordCrypter passwordCrypter;

    public User findById(Long id) {
        return userRepository.findById(id).isPresent()?userRepository.findById(id).get():null;
    }

    public void signUp(User user) throws RuntimeException {
        if(user.getRole()==null) {
            LOGGER.error(ErrorMessage.NOT_ROLE_FOUND);
            throw new InvalidRoleException(ErrorMessage.NOT_ROLE_FOUND);
        }
        User existingUser = userRepository.findByUsername(user.getUsername());
        if(existingUser!=null) {
            LOGGER.error(ErrorMessage.INVALID_USERNAME);
            throw new InvalidUsernameException(ErrorMessage.INVALID_USERNAME);
        }
        user.setPassword(passwordCrypter.hashPassword(user.getPassword()));
        userRepository.save(user);
        LOGGER.info(user, SuccesMessage.SUCCES_SIGN_UP);
    }

    public Token authenticateWithCredentials(User user) throws RuntimeException {
        if(!user.hasCredentials()) {
            LOGGER.error(ErrorMessage.REQUIRED_USERNAME_PASSWORD);
            throw new CredentialsRequiredException(ErrorMessage.REQUIRED_USERNAME_PASSWORD);
        }
        User existingUser = userRepository.findByUsername(user.getUsername());
        if(existingUser==null || (!passwordCrypter.checkPass(user.getPassword(), existingUser.getPassword()))) {
            LOGGER.error(ErrorMessage.INVALID_CREDENTIALS);
            throw new InvalidCredentialsException(ErrorMessage.INVALID_CREDENTIALS);
        }
        Token token = authenticator.obtainToken(existingUser);
        return token;
    }


    public Long authenticateWithToken(String token) throws NotAuthorizedException {
        return authenticator.authenticateWithToken(token);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public List<User> findByFilter(String filter) {
        return userRepository.findByFilter(filter);
    }

}
