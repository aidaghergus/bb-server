package ism.ase.ro.bb.service.impl;

import ism.ase.ro.bb.model.entity.Address;
import ism.ase.ro.bb.model.entity.User;
import ism.ase.ro.bb.repository.AddressRepository;
import ism.ase.ro.bb.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    AddressRepository addressRepository;

    @Override
    public void save(Address address) {
        addressRepository.save(address);
    }

    @Override
    public List<Address> findByUser(User user) {
        return addressRepository.findAllByUser(user);
    }
}
