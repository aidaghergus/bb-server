package ism.ase.ro.bb.service.impl;

import ism.ase.ro.bb.mapper.AgreementMapper;
import ism.ase.ro.bb.model.dto.AgreementComplexDTO;
import ism.ase.ro.bb.model.entity.Agreement;
import ism.ase.ro.bb.model.entity.Item;
import ism.ase.ro.bb.model.entity.User;
import ism.ase.ro.bb.repository.AgreementRepository;
import ism.ase.ro.bb.repository.ItemRepository;
import ism.ase.ro.bb.repository.OfferRepository;
import ism.ase.ro.bb.service.AgreementService;
import ism.ase.ro.bb.utils.ItemStatus;
import ism.ase.ro.bb.utils.TransactionStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AgreementServiceImpl implements AgreementService {

    @Autowired
    AgreementRepository agreementRepository;

    @Autowired
    OfferRepository offerRepository;

    @Autowired
    ItemRepository itemRepository;

    public List<Long> findOfferIds(User user){
        List<Long> offerIds=new ArrayList<>();
        if(offerRepository.findAllByUser(user)!=null)
            offerRepository.findAllByUser(user).forEach(x -> offerIds.add(x.getId()));
        return offerIds;
    }

    @Override
    public void save(Agreement agreement) {
        agreementRepository.save(agreement);
    }

    @Override
    public Agreement findById(Long agreementId) {
        return agreementRepository.findById(agreementId).isPresent()?agreementRepository.findById(agreementId).get():null;
    }

    @Override
    public void signAgreement(Long agreementId) {
        Agreement agreement=agreementRepository.findById(agreementId).isPresent()?agreementRepository.findById(agreementId).get():null;
        if(agreement!=null)
        {
            agreement.setStatus(TransactionStatus.FINISHED);
            agreement.setAccord("Between soliciter user: "+offerRepository.findById(agreement.getSolicitor()).get().getUser().getUsername()
                    + " with product: "+offerRepository.findById(agreement.getSolicitor()).get().getItem().getName()
                    + " and solicitee: "+offerRepository.findById(agreement.getSolicitee()).get().getUser().getUsername()
                    + " with product: "+offerRepository.findById(agreement.getSolicitee()).get().getItem().getName()
            );

            Item soliciorItem=offerRepository.findById(agreement.getSolicitor()).get().getItem();
            soliciorItem.setStatus(ItemStatus.TRADED);
            Item soliciteeItem=offerRepository.findById(agreement.getSolicitee()).get().getItem();
            soliciteeItem.setStatus(ItemStatus.TRADED);

            itemRepository.save(soliciorItem);
            itemRepository.save(soliciteeItem);

            agreementRepository.save(agreement);
        }
    }

    @Override
    public void denyAgreement(Long agreementId) {
        Agreement agreement=agreementRepository.findById(agreementId).isPresent()?agreementRepository.findById(agreementId).get():null;
        if(agreement!=null) {
            agreement.setStatus(TransactionStatus.DENIED);
            Item soliciorItem=offerRepository.findById(agreement.getSolicitor()).get().getItem();
            soliciorItem.setStatus(ItemStatus.AVAILABLE);
            Item soliciteeItem=offerRepository.findById(agreement.getSolicitee()).get().getItem();
            soliciteeItem.setStatus(ItemStatus.AVAILABLE);
            itemRepository.save(soliciorItem);
            itemRepository.save(soliciteeItem);
            agreementRepository.save(agreement);
        }

    }

    @Override
    public List<Agreement> findPendingAgreementsForUser(User user) {
        return agreementRepository.findAllBySoliciteeInAndStatus(findOfferIds(user), TransactionStatus.INITIATED);
    }

    @Override
    public List<Agreement> findAcceptedAgreementsForUser(User user) {
        return agreementRepository.findAllBySoliciteeInAndStatus(findOfferIds(user), TransactionStatus.FINISHED);
    }

    @Override
    public List<Agreement> findSolicitedPendingAgreementsForUser(User user) {
        return agreementRepository.findAllBySolicitorInAndStatus(findOfferIds(user), TransactionStatus.INITIATED);
    }

    @Override
    public List<Agreement> findSolicitedAndAcceptedAgreementsForUser(User user) {
        return agreementRepository.findAllBySolicitorInAndStatus(findOfferIds(user), TransactionStatus.FINISHED);
    }
}
