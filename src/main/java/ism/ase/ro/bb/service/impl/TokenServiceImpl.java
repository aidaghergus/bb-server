package ism.ase.ro.bb.service.impl;

import ism.ase.ro.bb.model.entity.Token;
import ism.ase.ro.bb.model.entity.User;
import ism.ase.ro.bb.repository.TokenRepository;
import ism.ase.ro.bb.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenRepository tokenRepository;

    public List<Token> findByUser(User user) {
        return tokenRepository.findByUser(user);
    }

    public Token findByToken(String token) {
        return tokenRepository.findByToken(token);
    }

    public User getUser(String token) {
        Token existingToken = findByToken(token);
        return existingToken.getUser();
    }

    public void save(Token token) {
        tokenRepository.save(token);
    }

    public void delete(Token token) {
        tokenRepository.delete(token);
    }
}
