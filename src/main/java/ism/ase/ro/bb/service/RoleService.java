package ism.ase.ro.bb.service;


import ism.ase.ro.bb.model.entity.Role;

import java.util.List;

public interface RoleService {

    List<Role> findAll();
}
