package ism.ase.ro.bb.service;

import ism.ase.ro.bb.model.entity.Item;
import ism.ase.ro.bb.model.entity.Offer;
import ism.ase.ro.bb.model.entity.User;

import java.util.List;

public interface OfferService {
    void save(Offer offer) throws Exception;

    void delete(Offer offer);

    Offer findById(Long id);

    List<Offer> findByUser(User user);

    List<Offer> findByItem(Item item);

}
