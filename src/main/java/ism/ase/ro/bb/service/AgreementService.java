package ism.ase.ro.bb.service;

import ism.ase.ro.bb.model.entity.Agreement;
import ism.ase.ro.bb.model.entity.User;

import java.util.List;

public interface AgreementService {
    void save(Agreement agreement);

    Agreement findById(Long agreementId);

    void signAgreement(Long agreementId);

    void denyAgreement(Long agreementId);

    List<Agreement> findPendingAgreementsForUser(User user);

    List<Agreement> findAcceptedAgreementsForUser(User user);

    List<Agreement> findSolicitedPendingAgreementsForUser(User user);

    List<Agreement> findSolicitedAndAcceptedAgreementsForUser(User user);

}
