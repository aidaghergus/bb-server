package ism.ase.ro.bb.service;

import ism.ase.ro.bb.model.entity.Comment;
import ism.ase.ro.bb.model.entity.Item;
import ism.ase.ro.bb.model.entity.Offer;
import ism.ase.ro.bb.model.entity.User;

import java.util.List;

public interface ItemService {

    void save(Item item);

    Item findById(Long itemId);

    List<Item> findAll();

    List<Item> findAllByUser(User user);

    List<Item> findAllAvailableByUser(User user);

}
