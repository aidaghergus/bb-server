package ism.ase.ro.bb.service;


import ism.ase.ro.bb.model.entity.Token;
import ism.ase.ro.bb.model.entity.User;

import java.util.List;

public interface TokenService {

    List<Token> findByUser(User user);

    Token findByToken(String token);

    User getUser(String token);

    void save(Token token);

    void delete(Token token);

}
