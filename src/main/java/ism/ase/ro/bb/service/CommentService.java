package ism.ase.ro.bb.service;

import ism.ase.ro.bb.model.entity.Comment;
import ism.ase.ro.bb.model.entity.User;

import java.util.List;

public interface CommentService {

    void save(Comment comment);

    List<Comment> findByUser(User user);

    List<Comment> findByItem(Long itemId);


}
