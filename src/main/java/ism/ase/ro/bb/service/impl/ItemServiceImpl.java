package ism.ase.ro.bb.service.impl;

import ism.ase.ro.bb.model.entity.Item;
import ism.ase.ro.bb.model.entity.User;
import ism.ase.ro.bb.service.ItemService;
import ism.ase.ro.bb.utils.ItemStatus;
import org.springframework.beans.factory.annotation.Autowired;
import ism.ase.ro.bb.repository.ItemRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    ItemRepository itemRepository;

    @Override
    public void save(Item item) {
        itemRepository.save(item);
    }

    @Override
    public Item findById(Long itemId) {
        return itemRepository.findById(itemId).isPresent()?itemRepository.findById(itemId).get():null;
    }

    @Override
    public List<Item> findAll() {
        return itemRepository.findAll();
    }

    @Override
    public List<Item> findAllByUser(User user) {
        return itemRepository.findAllByUser(user);
    }

    @Override
    public List<Item> findAllAvailableByUser(User user) {
        return itemRepository.findAllByUserAndStatus(user, ItemStatus.AVAILABLE);
    }

}
