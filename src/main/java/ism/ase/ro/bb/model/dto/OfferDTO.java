package ism.ase.ro.bb.model.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class OfferDTO {

    private Long id;

    private Long userId;

    private Long itemId;

    private String offerMessage;
}
