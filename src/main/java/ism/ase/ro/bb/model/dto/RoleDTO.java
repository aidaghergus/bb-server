package ism.ase.ro.bb.model.dto;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class RoleDTO implements Serializable {

    private String roleName;
}
