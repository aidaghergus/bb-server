package ism.ase.ro.bb.model.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class AddressDTO {
    private String address;
    private Long userId;
}
