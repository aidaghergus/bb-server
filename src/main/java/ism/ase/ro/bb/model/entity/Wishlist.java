/*package ism.ase.ro.bb.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity(name="wishlist")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Wishlist implements Serializable {

    @Id
    @Column(name="item_id", nullable = false)
    private Long item;

    @Id
    @Column(name="user_id", nullable = false)
    private Long user;

}*/
