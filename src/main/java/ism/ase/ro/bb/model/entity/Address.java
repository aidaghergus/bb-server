package ism.ase.ro.bb.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="addresses")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    @Id
    @Column(name="eth_address")
    private String address;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;
}
