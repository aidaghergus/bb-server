package ism.ase.ro.bb.model.entity;

import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity(name="users")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="user_id")
    private Long id;

    @Column(name="first_name", nullable=false)
    private String firstName;

    @Column(name="last_name", nullable=false)
    private String lastName;

    @Column(name="username", nullable=false)
    private String username;

    @Column(name="password", nullable=false)
    private String password;

    @Column(name="enabled", nullable=false)
    private boolean enabled;

    @Column(name="email", nullable=false)
    private String email;

    @Column(name="phone", nullable=false)
    private String phone;

    @Column(name="gender", nullable=false)
    private Character gender;

    @ManyToOne
    @JoinColumn(name="role")
    private Role role;

    @ToString.Exclude
    @OneToMany(mappedBy = "user")
    private List<Offer> offers;

    @ToString.Exclude
    @OneToMany(mappedBy = "user")
    private List<Address> addresses;

    @ToString.Exclude
    @OneToMany(mappedBy = "user")
    private List<Comment> comments;

    @ToString.Exclude
    @OneToMany(mappedBy = "user")
    private List<Item> items;


    public boolean hasCredentials() {
        if(this.username!=null&&this.password!=null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        User user = (User) o;

        return id != null ? id.equals(user.id) : user.id == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    public String getFullName() {
        return new StringBuilder().append(this.firstName).append(" ").append(this.lastName).toString();
    }


}
