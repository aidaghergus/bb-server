package ism.ase.ro.bb.model.entity;

import ism.ase.ro.bb.utils.ItemStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity(name="items")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Item {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="item_id")
    private Long id;

    @Column(nullable = false, name="status")
    @Enumerated(EnumType.ORDINAL)
    private ItemStatus status;

    @Column(name="name", nullable=false)
    private String name;

    @Column(name="overview", nullable=false)
    private String description;

    @Column(name="added_at")
    private Date addDate;

    @Column(name="photo_url")
    private String photoUrl;

    @ManyToOne
    @JoinColumn(nullable = false, name="user_id")
    @ToString.Exclude
    private User user;

    @LazyCollection(LazyCollectionOption.FALSE)
    @NotFound(
            action = NotFoundAction.IGNORE)
    @OneToMany(mappedBy = "item")
    private List<Comment> comments;

}
