package ism.ase.ro.bb.model.dto;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class TokenDTO implements Serializable {

    private String token;
    private UserGeneralDTO user;
}
