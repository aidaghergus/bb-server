package ism.ase.ro.bb.model.dto;

import ism.ase.ro.bb.model.entity.User;
import ism.ase.ro.bb.utils.ItemStatus;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class ItemDTO {

    private Long id;

    private ItemStatus status;

    private String name;

    private String description;

    private Date addDate;

    private String photoUrl;

    private Long userId;

    private List<CommentDTO> commentList;

}
