package ism.ase.ro.bb.model.dto;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class CommentUserDTO implements Serializable {

    private Long id;

    private String commentDate;

    private String content;

    private ItemComDTO show;
}
