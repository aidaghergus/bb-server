package ism.ase.ro.bb.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="roles")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EnableAutoConfiguration
public class Role {

    @Id
    @Column(name="role")
    private String roleName;

    @Column(name="privilege",nullable=false)
    private String privilege;

}
