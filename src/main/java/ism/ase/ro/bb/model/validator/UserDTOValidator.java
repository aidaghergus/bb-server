package ism.ase.ro.bb.model.validator;

import ism.ase.ro.bb.errorhandling.error.ErrorMessage;
import ism.ase.ro.bb.errorhandling.error.ErrorUtils;
import ism.ase.ro.bb.errorhandling.exception.general.InvalidFieldsException;
import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.model.dto.UserDTO;

public class UserDTOValidator {

    private static final Logger LOGGER = new Logger(UserDTOValidator.class);

    public static void validate(UserDTO userDTO) {
        StringBuilder errors = new StringBuilder(ErrorMessage.NULL_ATRIBUTES);
        if(userDTO.getFirstName()==null||userDTO.getFirstName().trim().isEmpty()) {
            ErrorUtils.appendError(errors,"First Name");
        }
        if(userDTO.getLastName()==null||userDTO.getLastName().trim().isEmpty()) {
            ErrorUtils.appendError(errors,"Last Name");
        }
        if(userDTO.getUsername()==null||userDTO.getUsername().trim().isEmpty()) {
            ErrorUtils.appendError(errors,"Username");
        }
        if(userDTO.getPassword()==null||userDTO.getPassword().trim().isEmpty()) {
            ErrorUtils.appendError(errors,"Password");
        }
        if(userDTO.getEmail()==null||userDTO.getEmail().trim().isEmpty()) {
            ErrorUtils.appendError(errors,"Email");
        }
        if(userDTO.getPhone()==null||userDTO.getPhone().trim().isEmpty()) {
            ErrorUtils.appendError(errors,"Phone");
        }
        if(userDTO.getGender()==null) {
            ErrorUtils.appendError(errors,"Gender");
        }
        if(userDTO.getRole()==null||userDTO.getRole().trim().isEmpty()) {
            ErrorUtils.appendError(errors,"Role");
        }
        if(!errors.toString().equals(ErrorMessage.NULL_ATRIBUTES)) {
            LOGGER.error(errors.toString());
            throw new InvalidFieldsException(errors.toString());
        }
    }
}
