package ism.ase.ro.bb.model.dto;

import lombok.*;

import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class UserComDTO implements Serializable {
    protected Long id;
    protected String username;
}
