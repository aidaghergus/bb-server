package ism.ase.ro.bb.model.entity;


import net.minidev.json.annotate.JsonIgnore;

import javax.persistence.*;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Entity(name="tokens")
public class Token {

	    private static final String TOKEN_MARKER = "AUTH_";

		@Id
		@GeneratedValue(strategy= GenerationType.AUTO)
		@Column(name="token_id")
		@JsonIgnore
		private Long id;

		@Column(name="token", nullable=false)
	    private String token;

		@Column(name="expiration_date", nullable=false)
		@JsonIgnore
		private final Date expirationDate;

		@ManyToOne
		@JoinColumn(name="user_id")
		@JsonIgnore
		private User user;

		public Token() {
			super();
			expirationDate = generateExpirationDate(0);
		}

	    public Token(final int expiredDays) {
	        token = Token.TOKEN_MARKER + UUID.randomUUID().toString();
	        expirationDate = generateExpirationDate(expiredDays);
	    }

	    public Token(String token, long expirationDate) {
	        this.expirationDate = generateExpirationDate(expirationDate);
	        this.token = token;
	    }

	    private Date generateExpirationDate(final int expiredDays) {

	        final Calendar c = Calendar.getInstance();
	        c.setTime(new Date());
	        c.add(Calendar.DATE, expiredDays);
	        return c.getTime();
	    }

	    private Date generateExpirationDate(long expirationDate) {

	        final Calendar c = Calendar.getInstance();
	        c.setTimeInMillis(expirationDate);
	        return c.getTime();
	    }

	    public long getExpirationTime() {

	        return expirationDate.getTime();
	    }


	    public boolean isExpired() {

	        return new Date().after(expirationDate);
	    }

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}

		public Date getExpirationDate() {
			return expirationDate;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}
	}
