package ism.ase.ro.bb.model.dto;


import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class CommentDTO {

    private Long id;

    private String commentDate;

    private String content;

    private UserComDTO user;

    private ItemComDTO item;

}
