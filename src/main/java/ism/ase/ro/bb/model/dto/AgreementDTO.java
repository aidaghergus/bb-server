package ism.ase.ro.bb.model.dto;

import ism.ase.ro.bb.utils.TransactionStatus;
import lombok.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class AgreementDTO {
    private Long id;

    private Long solicitorOfferID;

    private Long soliciteeOfferId;

    private String accord;

    private Date date;

    private TransactionStatus status;
}
