package ism.ase.ro.bb.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name="offers")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Offer {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="offer_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name="user_id", nullable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name="item_id", nullable = false)
    private Item item;

    @Column(name="offer_message", nullable = false)
    private String offerMessage;
}
