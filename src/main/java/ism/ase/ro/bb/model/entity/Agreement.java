package ism.ase.ro.bb.model.entity;

import ism.ase.ro.bb.utils.TransactionStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity(name="agreements")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Agreement {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="agreement_id")
    private Long id;

    @Column(name="solicitor_offer")
    private Long solicitor;

    @Column(name="solicitee_offer")
    private Long solicitee;

    @Column(name="accord")
    private String accord;

    @Column(name="date")
    private Date date;

    @Enumerated(EnumType.ORDINAL)
    @Column(name="status")
    private TransactionStatus status;
}
