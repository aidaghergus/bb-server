package ism.ase.ro.bb.model.dto;

import ism.ase.ro.bb.utils.ItemStatus;
import lombok.*;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class ItemComDTO {

    private Long id;

    private String name;

    private Date addDate;

}
