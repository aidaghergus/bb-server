package ism.ase.ro.bb.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserGeneralDTO implements Serializable {

    private static final long serialVersionUID = 8746729891518659733L;

    protected Long id;
    protected String firstName;
    protected String lastName;
    protected String email;
    protected String username;
    protected String phone;
    protected Character gender;
    protected String role;

}
