package ism.ase.ro.bb.model.dto;


import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class OfferComplexDTO {

    private Long id;

    private UserGeneralDTO userGeneralDTO;

    private ItemDTO itemDTO;

    private String offerMessage;
}
