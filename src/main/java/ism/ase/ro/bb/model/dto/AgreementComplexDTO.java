package ism.ase.ro.bb.model.dto;

import ism.ase.ro.bb.utils.TransactionStatus;
import lombok.*;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
public class AgreementComplexDTO {
    private Long id;

    private OfferComplexDTO solicitorOffer;

    private OfferComplexDTO soliciteeOffer;

    private String accord;

    private Date date;

    private TransactionStatus status;
}
