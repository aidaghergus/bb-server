package ism.ase.ro.bb.errorhandling.exception.general;

public class BadRequestException extends RuntimeException {

    public BadRequestException(final String error) {
        super(error);
    }

}
