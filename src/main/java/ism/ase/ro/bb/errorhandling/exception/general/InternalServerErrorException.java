package ism.ase.ro.bb.errorhandling.exception.general;

public class InternalServerErrorException extends RuntimeException {

    public InternalServerErrorException(String message) {
        super(message);
    }
}
