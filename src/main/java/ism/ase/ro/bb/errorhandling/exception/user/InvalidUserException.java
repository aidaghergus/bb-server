package ism.ase.ro.bb.errorhandling.exception.user;

public class InvalidUserException extends RuntimeException {

    public InvalidUserException(String message) {
        super(message);
    }
}
