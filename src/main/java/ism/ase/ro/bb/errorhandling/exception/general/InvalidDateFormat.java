package ism.ase.ro.bb.errorhandling.exception.general;

public class InvalidDateFormat extends RuntimeException {

    public InvalidDateFormat(final String error) {
        super(error);
    }
}
