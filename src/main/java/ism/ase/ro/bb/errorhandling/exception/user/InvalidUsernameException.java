package ism.ase.ro.bb.errorhandling.exception.user;

public class InvalidUsernameException extends RuntimeException {

    public InvalidUsernameException(final String error) {
        super(error);
    }
}
