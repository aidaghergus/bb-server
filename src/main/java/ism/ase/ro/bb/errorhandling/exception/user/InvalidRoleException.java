package ism.ase.ro.bb.errorhandling.exception.user;

public class InvalidRoleException extends RuntimeException {

    public InvalidRoleException(final String error) {
        super(error);
    }
}
