package ism.ase.ro.bb.errorhandling.exception.general;

public class InvalidFieldsException  extends RuntimeException {

    public InvalidFieldsException(final String error) {
        super(error);
    }
}
