package ism.ase.ro.bb.errorhandling.exception.user;

public class NotAuthorizedException extends RuntimeException {

    public NotAuthorizedException(final String error) {
        super(error);
    }
}
