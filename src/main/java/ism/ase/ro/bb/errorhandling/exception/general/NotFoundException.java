package ism.ase.ro.bb.errorhandling.exception.general;

public class NotFoundException extends RuntimeException{

    public NotFoundException(String error) {
        super(error);
    }
}
