package ism.ase.ro.bb.errorhandling.exception.user;

public class InvalidCredentialsException  extends RuntimeException {

    public InvalidCredentialsException(final String error) {
        super(error);
    }
}
