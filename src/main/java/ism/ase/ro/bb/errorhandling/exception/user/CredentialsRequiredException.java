package ism.ase.ro.bb.errorhandling.exception.user;

public class CredentialsRequiredException extends RuntimeException {

    public CredentialsRequiredException(final String error) {
        super(error);
    }
}
