package ism.ase.ro.bb.mapper;

import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.model.dto.ItemComDTO;
import ism.ase.ro.bb.model.dto.ItemDTO;
import ism.ase.ro.bb.model.entity.Item;

import java.util.stream.Collectors;

public class ItemMapper {

    private static final Logger LOGGER = new Logger(ItemMapper.class);

    private ItemMapper(){}

    public static Item mapToEntity(ItemDTO itemDTO, RepositoryMapper mapper){
        Item item=new Item();
        item.setName(itemDTO.getName());
        item.setDescription(itemDTO.getDescription());
        item.setPhotoUrl(itemDTO.getPhotoUrl());
        item.setAddDate(itemDTO.getAddDate());
        if(itemDTO.getStatus()!=null)
            item.setStatus(itemDTO.getStatus());
        if(itemDTO.getUserId()!=null)
            item.setUser(mapper.getUserRepository().findById(itemDTO.getUserId()).isPresent()?mapper.getUserRepository().findById(itemDTO.getUserId()).get():null);
        if(itemDTO.getCommentList()!= null)
            item.setComments(itemDTO.getCommentList().stream().map(commentDTO -> CommentMapper.mapToEntity(commentDTO,mapper)).collect(Collectors.toList()));
        return item;
    }

    public static ItemDTO mapToDTO(Item item){
        return ItemDTO.builder()
                .id(item.getId())
                .name(item.getName())
                .description(item.getDescription())
                .addDate(item.getAddDate())
                .photoUrl(item.getPhotoUrl())
                .status(item.getStatus())
                .userId(item.getUser().getId())
/*
                .commentList(item.getComments()!=null?item.getComments().stream().map(CommentMapper::mapToDTO).collect(Collectors.toList()):null)
*/
                .build();
    }

    public static Item mapToEntity(ItemComDTO itemComDTO, RepositoryMapper repositoryMapper){
        return repositoryMapper.getItemRepository().findById(itemComDTO.getId()).get();
    }

    public static ItemComDTO mapToItemComDTO(Item item){
        return new ItemComDTO(item.getId(),item.getName(),item.getAddDate());
    }
}
