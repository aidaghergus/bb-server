package ism.ase.ro.bb.mapper;

import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.model.dto.AgreementComplexDTO;
import ism.ase.ro.bb.model.dto.AgreementDTO;
import ism.ase.ro.bb.model.entity.Agreement;
import ism.ase.ro.bb.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;

public class AgreementMapper {
    private static final Logger LOGGER = new Logger(AgreementMapper.class);

    @Autowired
    OfferService offerService;

    private AgreementMapper(){}

    public static Agreement mapToEntity(AgreementDTO agreementDTO, RepositoryMapper mapper){
        Agreement agreement=new Agreement();
        agreement.setAccord(agreementDTO.getAccord());
        agreement.setDate(agreementDTO.getDate());
        agreement.setSolicitee(agreementDTO.getSoliciteeOfferId());
        agreement.setSolicitor(agreementDTO.getSolicitorOfferID());
        agreement.setStatus(agreementDTO.getStatus());
        return agreement;
    }

    public static AgreementDTO mapToDTO(Agreement agreement){
        return AgreementDTO.builder()
                .id(agreement.getId())
                .accord(agreement.getAccord())
                .soliciteeOfferId(agreement.getSolicitee())
                .solicitorOfferID(agreement.getSolicitor())
                .date(agreement.getDate())
                .status(agreement.getStatus())
                .build();
    }

    public static AgreementComplexDTO mapToComplexDTO(Agreement agreement, RepositoryMapper mapper){
        return AgreementComplexDTO.builder()
                .id(agreement.getId())
                .accord(agreement.getAccord())
                .soliciteeOffer(OfferMapper.mapToComplexDTO(mapper.getOfferRepository().findById(agreement.getSolicitee()).isPresent()?mapper.getOfferRepository().findById(agreement.getSolicitee()).get():null, mapper))
                .solicitorOffer(OfferMapper.mapToComplexDTO(mapper.getOfferRepository().findById(agreement.getSolicitor()).isPresent()?mapper.getOfferRepository().findById(agreement.getSolicitor()).get():null, mapper))
                .date(agreement.getDate())
                .status(agreement.getStatus())
                .build();
    }

}
