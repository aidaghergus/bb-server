package ism.ase.ro.bb.mapper;

import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.model.dto.OfferComplexDTO;
import ism.ase.ro.bb.model.dto.OfferDTO;
import ism.ase.ro.bb.model.entity.Offer;

public class OfferMapper {
    private static final Logger LOGGER = new Logger(OfferMapper.class);

    private OfferMapper(){}

    public static Offer mapToEntity(OfferDTO offerDTO, RepositoryMapper mapper){
        Offer offer=new Offer();
        offer.setItem(mapper.getItemRepository().findById(offerDTO.getItemId()).get());
        offer.setOfferMessage(offerDTO.getOfferMessage());
        offer.setUser(mapper.getUserRepository().findById(offerDTO.getUserId()).get());
        return offer;
    }

    public static OfferDTO mapToDTO(Offer offer){
        return OfferDTO.builder()
                .id(offer.getId())
                .offerMessage(offer.getOfferMessage())
                .userId(offer.getUser().getId())
                .itemId(offer.getItem().getId())
                .build();
    }

    public static OfferComplexDTO mapToComplexDTO(Offer offer, RepositoryMapper mapper){
        return OfferComplexDTO.builder()
                .id(offer.getId())
                .offerMessage(offer.getOfferMessage())
                .userGeneralDTO(UserMapper.mapToDTO(mapper.getUserRepository().findById(offer.getUser().getId()).get()))
                .itemDTO(ItemMapper.mapToDTO(mapper.getItemRepository().findById(offer.getItem().getId()).get()))
                .build();
    }
}
