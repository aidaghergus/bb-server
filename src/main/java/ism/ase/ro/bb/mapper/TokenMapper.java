package ism.ase.ro.bb.mapper;

import ism.ase.ro.bb.model.dto.TokenDTO;
import ism.ase.ro.bb.model.entity.Token;

public class TokenMapper {

    private TokenMapper() {

    }

    public static Token mapToEntity(TokenDTO tokenDto, RepositoryMapper mapper) {
        return mapper.getTokenRepository().findByToken(tokenDto.getToken());
    }

    public static TokenDTO mapToDTO(Token token) {
        TokenDTO tokenDTO = TokenDTO.builder().token(token.getToken()).user(UserMapper.mapToUserGeneralDTO(token.getUser())).build();
        return tokenDTO;
    }

}
