package ism.ase.ro.bb.mapper;


import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.model.dto.CommentDTO;
import ism.ase.ro.bb.model.dto.CommentUserDTO;
import ism.ase.ro.bb.model.entity.Comment;
import ism.ase.ro.bb.utils.DateFormatter;

public class CommentMapper {
    private static final Logger LOGGER = new Logger(CommentMapper.class);

    private CommentMapper(){}

    public static Comment mapToEntity(CommentDTO commentDTO, RepositoryMapper mapper){
        Comment comment=new Comment();
        //comment.setId(commentDTO.getId());
        comment.setContent(commentDTO.getContent());
        /*try {
            comment.setReviewDate(DateFormatter.formatter.parse(commentDTO.getCommentDate()));
        } catch (ParseException e) {
            LOGGER.error(ErrorMessage.INVALID_DATE_FORMAT);
            e.printStackTrace();
        }*/

        comment.setUser(UserMapper.mapToEntity(commentDTO.getUser(),mapper));

        return comment;
    }


    public static CommentDTO mapToDTO(Comment comment){
        return CommentDTO.builder()
                .id(comment.getId())
                .content(comment.getContent())
                .commentDate(DateFormatter.formatter.format(comment.getReviewDate()))
                .user(UserMapper.mapToUserComDTO(comment.getUser()))
                .item(ItemMapper.mapToItemComDTO(comment.getItem()))
                .build();
    }

    public static CommentUserDTO mapToCommentUserDTO(Comment comment){
        return new CommentUserDTO(comment.getId(),comment.getReviewDate().toString(),comment.getContent(),ItemMapper.mapToItemComDTO(comment.getItem()));
    }

    public static Comment mapToEntity(CommentUserDTO commentUserDTO, RepositoryMapper mapper){
        return mapper.getCommentRepository().findById(commentUserDTO.getId()).get();
    }

}
