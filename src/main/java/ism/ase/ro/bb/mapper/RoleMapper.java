package ism.ase.ro.bb.mapper;

import ism.ase.ro.bb.model.dto.RoleDTO;
import ism.ase.ro.bb.model.entity.Role;

public class RoleMapper {

    private RoleMapper() {

    }

    public static Role mapToEntity(RoleDTO roleDTO, RepositoryMapper mapper) {
        return mapper.getRoleRepository().findByRoleName(roleDTO.getRoleName());
    }

    public static RoleDTO mapToDTO(Role role) {
        return RoleDTO.builder().roleName(role.getRoleName()).build();
    }
}
