package ism.ase.ro.bb.mapper;

import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.model.dto.AddressDTO;
import ism.ase.ro.bb.model.entity.Address;

public class AddressMapper {
    private static final Logger LOGGER = new Logger(AddressMapper.class);

    private AddressMapper(){}

    public static Address mapToEntity(AddressDTO addressDTO, RepositoryMapper mapper){
        Address address=new Address();
        address.setAddress(addressDTO.getAddress());
        address.setUser(mapper.getUserRepository().findById(addressDTO.getUserId()).get());
        return address;
    }

    public static AddressDTO mapToDTO(Address address){
        return AddressDTO.builder()
                .address(address.getAddress())
                .userId(address.getUser().getId())
                .build();
    }

}
