package ism.ase.ro.bb.repository;

import ism.ase.ro.bb.model.entity.Item;
import ism.ase.ro.bb.model.entity.User;
import ism.ase.ro.bb.utils.ItemStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ItemRepository extends JpaRepository<Item, Long> {

    List<Item> findAllByUser(User user);

    List<Item> findAllByUserAndStatus(User user, ItemStatus itemStatus);
}
