package ism.ase.ro.bb.repository;

import ism.ase.ro.bb.model.entity.Item;
import ism.ase.ro.bb.model.entity.Offer;
import ism.ase.ro.bb.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OfferRepository extends JpaRepository<Offer, Long> {
    List<Offer> findAllByUser(User user);
    List<Offer> findAllByItem(Item item);
}
