package ism.ase.ro.bb.repository;

import ism.ase.ro.bb.model.entity.Token;
import ism.ase.ro.bb.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {

    Token findByToken(String token);

    List<Token> findByUser(User user);
}
