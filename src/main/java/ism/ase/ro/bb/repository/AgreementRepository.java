package ism.ase.ro.bb.repository;

import ism.ase.ro.bb.model.entity.Agreement;
import ism.ase.ro.bb.utils.TransactionStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AgreementRepository extends JpaRepository<Agreement, Long> {
    List<Agreement> findAllBySoliciteeInAndStatus(List<Long> solicitee, TransactionStatus status);

    List<Agreement> findAllBySolicitorInAndStatus(List<Long> solicitor, TransactionStatus status);
}
