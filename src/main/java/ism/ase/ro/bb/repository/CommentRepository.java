package ism.ase.ro.bb.repository;

import ism.ase.ro.bb.model.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findAllByUserId(Long id);

    List<Comment> findAllByItemId(Long id);
}
