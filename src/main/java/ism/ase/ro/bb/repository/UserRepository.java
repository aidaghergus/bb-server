package ism.ase.ro.bb.repository;

import ism.ase.ro.bb.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    User findByUsernameAndPassword(String username, String password);

    @Query("SELECT u FROM users u WHERE UPPER(u.firstName) LIKE CONCAT('%',UPPER(:filter),'%')" +
            "OR UPPER(u.lastName) LIKE CONCAT('%',UPPER(:filter),'%')" +
            "OR UPPER(u.username) LIKE CONCAT('%',UPPER(:filter),'%')" +
            "OR UPPER(u.email) LIKE CONCAT('%',UPPER(:filter),'%')")
    List<User> findByFilter(@Param("filter") String filter);

}
