package ism.ase.ro.bb.repository;

import ism.ase.ro.bb.model.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {

    public Role findByRoleName(String roleName);


}
