package ism.ase.ro.bb.utils;

public enum ItemStatus {
    AVAILABLE, WAITING, TRADED
}
