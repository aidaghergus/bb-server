package ism.ase.ro.bb.utils;

public interface Constants {

    String AUTHORIZATION = "Authorization";
    String ROLE_ADMIN = "admin";
}
