package ism.ase.ro.bb.utils;

public enum TransactionStatus {
    INITIATED, FINISHED, DENIED
}
