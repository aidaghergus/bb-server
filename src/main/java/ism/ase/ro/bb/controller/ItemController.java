package ism.ase.ro.bb.controller;

import ism.ase.ro.bb.http.request.RequestType;
import ism.ase.ro.bb.logger.InfoMessage;
import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.mapper.ItemMapper;
import ism.ase.ro.bb.mapper.RepositoryMapper;
import ism.ase.ro.bb.mapper.UserMapper;
import ism.ase.ro.bb.model.dto.ItemDTO;
import ism.ase.ro.bb.model.dto.UserComDTO;
import ism.ase.ro.bb.model.entity.Item;
import ism.ase.ro.bb.model.entity.User;
import ism.ase.ro.bb.service.ItemService;
import ism.ase.ro.bb.service.UserService;
import ism.ase.ro.bb.utils.Constants;
import ism.ase.ro.bb.utils.DateFormatter;
import ism.ase.ro.bb.utils.ItemStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/item")
@CrossOrigin
public class ItemController {

    private static final Logger LOGGER = new Logger(ItemController.class);

    @Autowired
    UserService userService;

    @Autowired
    ItemService itemService;

    @Autowired
    RepositoryMapper repositoryMapper;

    @GetMapping
    public List<ItemDTO> getAllItems(){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_ALL_ITEMS);
        return itemService.findAll().stream().map(ItemMapper::mapToDTO).collect(Collectors.toList());
    }

    @GetMapping("/user/all/{userId}")
    public List<ItemDTO> getUserItems(@PathVariable Long userId){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_ALL_ITEMS_OF_USER);
        User user=userService.findById(userId);
        return itemService.findAllByUser(user).stream().map(ItemMapper::mapToDTO).collect(Collectors.toList());
    }

    @GetMapping("/user/available/{userId}")
    public List<ItemDTO> getUserAvailableItems(@PathVariable Long userId){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_ALL_ITEMS_OF_USER);
        User user=userService.findById(userId);
        return itemService.findAllAvailableByUser(user).stream().map(ItemMapper::mapToDTO).collect(Collectors.toList());
    }

    @GetMapping("/id/{itemId}")
    public ItemDTO getItem(@PathVariable Long itemId){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_ITEM_BY_ID);
        Item item=itemService.findById(itemId);
        return ItemMapper.mapToDTO(item);
    }

    @PostMapping
    public void postItem(@RequestHeader(value = Constants.AUTHORIZATION) String token, @RequestBody ItemDTO itemDTO){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.ADD_ITEM);
        Long userId=userService.authenticateWithToken(token);
        itemDTO.setUserId(userId);
        //date, status
        Item itemToAdd=ItemMapper.mapToEntity(itemDTO,repositoryMapper);
        itemToAdd.setAddDate(new Date());
        itemToAdd.setStatus(ItemStatus.AVAILABLE);

        itemService.save(itemToAdd);
    }
}
