package ism.ase.ro.bb.controller;

import ism.ase.ro.bb.http.request.RequestType;
import ism.ase.ro.bb.logger.InfoMessage;
import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.mapper.OfferMapper;
import ism.ase.ro.bb.mapper.RepositoryMapper;
import ism.ase.ro.bb.model.dto.OfferDTO;
import ism.ase.ro.bb.model.entity.Offer;
import ism.ase.ro.bb.service.AgreementService;
import ism.ase.ro.bb.service.ItemService;
import ism.ase.ro.bb.service.OfferService;
import ism.ase.ro.bb.service.UserService;
import ism.ase.ro.bb.utils.Constants;
import ism.ase.ro.bb.utils.ItemStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/offer")
@CrossOrigin
public class OfferController {
    private static final Logger LOGGER = new Logger(OfferController.class);

    @Autowired
    UserService userService;

    @Autowired
    OfferService offerService;

    @Autowired
    RepositoryMapper repositoryMapper;

    @PostMapping
    public OfferDTO addSolicitorOffer(@RequestHeader(value = Constants.AUTHORIZATION) String token, @RequestBody OfferDTO offerDTO){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.ADD_SOLICITOR_OFFER);
        Long userId=userService.authenticateWithToken(token);
        offerDTO.setUserId(userId);
        Offer offer=OfferMapper.mapToEntity(offerDTO,repositoryMapper);
        try {
            offerService.save(offer);
            return OfferMapper.mapToDTO(offer);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @PostMapping("/solicitee")
    public OfferDTO addSoliciteeOffer(@RequestHeader(value = Constants.AUTHORIZATION) String token, @RequestBody OfferDTO offerDTO){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.ADD_SOLICITEE_OFFER);

        userService.authenticateWithToken(token);
        Offer offer=OfferMapper.mapToEntity(offerDTO,repositoryMapper);
        try {
            offerService.save(offer);
            return OfferMapper.mapToDTO(offer);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
