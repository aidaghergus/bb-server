package ism.ase.ro.bb.controller;

import ism.ase.ro.bb.http.request.RequestType;
import ism.ase.ro.bb.logger.InfoMessage;
import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.mapper.RoleMapper;
import ism.ase.ro.bb.model.dto.RoleDTO;
import ism.ase.ro.bb.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class RoleController {

    private static final Logger LOGGER = new Logger(RoleController.class);

    @Autowired
    RoleService roleService;

    @GetMapping("/roles")
    public List<RoleDTO> getAllRoles() {
        LOGGER.info(InfoMessage.REQUEST_RECEIVE + RequestType.GET_ROLES);
        return roleService.findAll().stream().map(RoleMapper::mapToDTO).collect(Collectors.toList());
    }




}
