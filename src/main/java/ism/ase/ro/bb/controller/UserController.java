package ism.ase.ro.bb.controller;

import ism.ase.ro.bb.http.request.RequestType;
import ism.ase.ro.bb.logger.InfoMessage;
import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.mapper.RepositoryMapper;
import ism.ase.ro.bb.mapper.UserMapper;
import ism.ase.ro.bb.model.dto.UserGeneralDTO;
import ism.ase.ro.bb.model.entity.User;
import ism.ase.ro.bb.service.UserService;
import ism.ase.ro.bb.utils.Constants;
import ism.ase.ro.bb.utils.SuccesMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class UserController {

    private static final Logger LOGGER = new Logger(UserController.class);

    @Autowired
    RepositoryMapper repositoryMapper;

    @Autowired
    UserService userService;


    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/users")
    public List<UserGeneralDTO> findByFilter(@RequestHeader(value= Constants.AUTHORIZATION) String token, @RequestParam String filter) {
        LOGGER.info(filter, InfoMessage.REQUEST_RECEIVE + RequestType.FIND_USERS);
        userService.authenticateWithToken(token);
        List<UserGeneralDTO> userGeneralDTOList = userService.findByFilter(filter).stream().map(UserMapper::mapToUserGeneralDTO).filter(user -> !user.getRole().equals(Constants.ROLE_ADMIN)).collect(Collectors.toList());
        LOGGER.info(userGeneralDTOList, SuccesMessage.SEND_USERS);
        return userGeneralDTOList;
    }

    @GetMapping("/users/{userId}")
    public UserGeneralDTO findById(@PathVariable Long userId) {
        LOGGER.info(userId, InfoMessage.REQUEST_RECEIVE + RequestType.GET_USER_BY_ID);
        User user=userService.findById(userId);
        return UserMapper.mapToUserGeneralDTO(user);
    }

    @GetMapping("/validate")
    public Long validateToken(@RequestHeader(value= Constants.AUTHORIZATION) String token) {
        LOGGER.info(InfoMessage.REQUEST_RECEIVE + RequestType.VALIDATE_TOKEN);
        return userService.authenticateWithToken(token);
    }



}
