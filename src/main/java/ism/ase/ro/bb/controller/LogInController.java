package ism.ase.ro.bb.controller;


import ism.ase.ro.bb.http.request.RequestType;
import ism.ase.ro.bb.logger.InfoMessage;
import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.mapper.RepositoryMapper;
import ism.ase.ro.bb.mapper.TokenMapper;
import ism.ase.ro.bb.mapper.UserMapper;
import ism.ase.ro.bb.model.dto.TokenDTO;
import ism.ase.ro.bb.model.dto.UserDTO;
import ism.ase.ro.bb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class LogInController {

    private static final Logger LOGGER = new Logger(LogInController.class);

    @Autowired
    UserService userService;

    @Autowired
    RepositoryMapper repositoryMapper;

    @PostMapping("/login")
    public TokenDTO logIn(@RequestBody UserDTO userDTO) {
        LOGGER.info(userDTO, InfoMessage.REQUEST_RECEIVE + RequestType.LOG_IN);
        return TokenMapper.mapToDTO(userService.authenticateWithCredentials(UserMapper.mapToUserCredentials(userDTO,repositoryMapper)));
    }
}
