package ism.ase.ro.bb.controller;

import ism.ase.ro.bb.http.request.RequestType;
import ism.ase.ro.bb.logger.InfoMessage;
import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.mapper.RepositoryMapper;
import ism.ase.ro.bb.mapper.UserMapper;
import ism.ase.ro.bb.model.dto.UserDTO;
import ism.ase.ro.bb.model.entity.User;
import ism.ase.ro.bb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api")
@CrossOrigin
public class SignUpController {

    private static final Logger LOGGER = new Logger(SignUpController.class);

    @Autowired
    RepositoryMapper mapper;

    @Autowired
    UserService userService;

    @PostMapping("/signup")
    public void signUp(@RequestBody UserDTO userDTO) {
        LOGGER.info(userDTO, InfoMessage.REQUEST_RECEIVE + RequestType.SIGN_UP);
        User user = UserMapper.mapToEntity(userDTO,mapper);
        userService.signUp(user);
    }

}
