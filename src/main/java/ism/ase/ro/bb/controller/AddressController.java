package ism.ase.ro.bb.controller;

import ism.ase.ro.bb.http.request.RequestType;
import ism.ase.ro.bb.logger.InfoMessage;
import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.mapper.AddressMapper;
import ism.ase.ro.bb.mapper.RepositoryMapper;
import ism.ase.ro.bb.model.dto.AddressDTO;
import ism.ase.ro.bb.service.AddressService;
import ism.ase.ro.bb.service.UserService;
import ism.ase.ro.bb.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/api/address")
@CrossOrigin
public class AddressController {
    private static final Logger LOGGER = new Logger(AddressController.class);

    @Autowired
    UserService userService;

    @Autowired
    RepositoryMapper repositoryMapper;

    @Autowired
    AddressService addressService;

    @PostMapping
    public void addAddress(@RequestHeader(value = Constants.AUTHORIZATION) String token, @RequestBody AddressDTO addressDTO){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.ADD_ADDRESS);
        Long userId=userService.authenticateWithToken(token);
        addressDTO.setUserId(userId);
        addressService.save(AddressMapper.mapToEntity(addressDTO,repositoryMapper));
    }

}
