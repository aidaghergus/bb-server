package ism.ase.ro.bb.controller;


import ism.ase.ro.bb.http.request.RequestType;
import ism.ase.ro.bb.logger.InfoMessage;
import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.mapper.AgreementMapper;
import ism.ase.ro.bb.mapper.RepositoryMapper;
import ism.ase.ro.bb.model.dto.AgreementComplexDTO;
import ism.ase.ro.bb.model.dto.AgreementDTO;
import ism.ase.ro.bb.model.dto.ItemDTO;
import ism.ase.ro.bb.model.entity.Agreement;
import ism.ase.ro.bb.model.entity.User;
import ism.ase.ro.bb.service.AgreementService;
import ism.ase.ro.bb.service.OfferService;
import ism.ase.ro.bb.service.UserService;
import ism.ase.ro.bb.utils.Constants;
import ism.ase.ro.bb.utils.TransactionStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/agreement")
@CrossOrigin
public class AgreeementController {
    private static final Logger LOGGER = new Logger(AgreeementController.class);

    @Autowired
    UserService userService;

    @Autowired
    AgreementService agreementService;

    @Autowired
    OfferService offerService;

    @Autowired
    RepositoryMapper repositoryMapper;

    @PostMapping
    public AgreementDTO addAgreement(@RequestHeader(value = Constants.AUTHORIZATION) String token, @RequestBody AgreementDTO agreementDTO){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.ADD_AGREEMENT);

        Long userId=userService.authenticateWithToken(token);
        if(offerService.findById(agreementDTO.getSolicitorOfferID())==null || offerService.findById(agreementDTO.getSolicitorOfferID()).getUser().getId()!=userId) return null;
        Agreement agreement= AgreementMapper.mapToEntity(agreementDTO,repositoryMapper);
        agreement.setStatus(TransactionStatus.INITIATED);
        agreement.setDate(new Date());
        agreementService.save(agreement);
        return AgreementMapper.mapToDTO(agreement);
    }

    @PostMapping("/sign/{agreementId}")
    public void signAgreement(@RequestHeader(value = Constants.AUTHORIZATION) String token, @PathVariable Long agreementId){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.SIGN_AGREEMENT);
        Long userId=userService.authenticateWithToken(token);
        agreementService.signAgreement(agreementId);
    }

    @PostMapping("/deny/{agreementId}")
    public void denyAgreement(@RequestHeader(value = Constants.AUTHORIZATION) String token, @PathVariable Long agreementId){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.DENY_AGREEMENT);

        Long userId=userService.authenticateWithToken(token);
        agreementService.denyAgreement(agreementId);
    }

    @GetMapping("/pending")
    public List<AgreementComplexDTO> getPendingAgreements(@RequestHeader(value = Constants.AUTHORIZATION) String token){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_PENDING_AGREEMENTS);

        Long userId=userService.authenticateWithToken(token);
        User user=userService.findById(userId);
        List<Agreement> agreements=agreementService.findPendingAgreementsForUser(user);
        return agreements.stream().map(agreement -> AgreementMapper.mapToComplexDTO(agreement,repositoryMapper)).collect(Collectors.toList());
    }

    @GetMapping("/waiting")
    public List<AgreementComplexDTO> getWaitingAgreements(@RequestHeader(value = Constants.AUTHORIZATION) String token){
        LOGGER.info(InfoMessage.REQUEST_RECEIVE+ RequestType.GET_WAITING_AGREEMENTS);

        Long userId=userService.authenticateWithToken(token);
        User user=userService.findById(userId);
        List<Agreement> agreements=agreementService.findSolicitedPendingAgreementsForUser(user);
        return agreements.stream().map(agreement -> AgreementMapper.mapToComplexDTO(agreement,repositoryMapper)).collect(Collectors.toList());
    }



}
