package ism.ase.ro.bb.logger;

public interface InfoMessage {
    String REQUEST_RECEIVE = "Request received for ";
    String TOKEN_OBTAINED = "Token obtained: ";
    String SUCCESFULLY_AUTH_TOKEN = "Succesfully authentication with token";
}
