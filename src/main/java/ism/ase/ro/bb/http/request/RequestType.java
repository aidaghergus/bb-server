package ism.ase.ro.bb.http.request;

public enum RequestType {

    SIGN_UP ("SIGN UP"),
    LOG_IN("LOG IN"),
    FIND_USERS("FIND_USERS"),
    GET_USER_BY_ID("GET_USER_BY_ID"),
    GET_ALL_ITEMS("GET ALL ITEMS"),
    GET_ALL_ITEMS_OF_USER("GET ALL ITEMS OF USER"),
    GET_ITEM_BY_ID("GET ITEM BY ID"),
    ADD_ITEM("ADD ITEMS"),
    VALIDATE_TOKEN("VALIDATE TOKEN"),
    GET_ITEM_COMMENTS("GET COMMENTS BY ITEM"),
    GET_ROLES("GET ROLES"),
    GET_USER_COMMENTS("GET USER COMMENTS"),
    POST_COMMENT("POST COMMENT"),
    ADD_ADDRESS("ADD ADDRESS"),
    GET_WAITING_AGREEMENTS("GET WAITING AGREEMENTS"),
    GET_PENDING_AGREEMENTS("GET PENDING AGREEMENTS"),
    DENY_AGREEMENT("DENY_AGREEMENT"),
    SIGN_AGREEMENT("SIGN_AGREEMENT"),
    ADD_AGREEMENT("ADD_AGREEMENT"),
    ADD_SOLICITOR_OFFER("ADD SOLICITOR OFFER"),
    ADD_SOLICITEE_OFFER("ADD SOLICITEE OFFER");


    private final String label;

    private RequestType(String label) {
        this.label=label;
    }

    public String getLabel() {
        return this.label;
    }

    @Override
    public String toString() {
        return this.label;
    }
}
