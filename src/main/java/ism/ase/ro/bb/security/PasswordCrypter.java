package ism.ase.ro.bb.security;


import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;


@Component
public class PasswordCrypter {
    public String hashPassword(String password) throws RuntimeException {
            return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public Boolean checkPass(String text, String pass){
        return BCrypt.checkpw(text, pass);
    }
}
