package ism.ase.ro.bb.security;

import ism.ase.ro.bb.errorhandling.error.ErrorMessage;
import ism.ase.ro.bb.errorhandling.exception.user.NotAuthorizedException;
import ism.ase.ro.bb.logger.InfoMessage;
import ism.ase.ro.bb.logger.Logger;
import ism.ase.ro.bb.model.entity.Token;
import ism.ase.ro.bb.model.entity.User;
import ism.ase.ro.bb.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


import java.util.List;

@Component
@PropertySource("classpath:bb.properties")
public class Authenticator {

    private static final Logger LOGGER = new Logger(Authenticator.class);

    @Value("${users.exiredDays}")
    private int expiredDays;

    @Autowired
    private TokenService tokenService;

    public Token obtainToken(User user) {
        List<Token> tokens = tokenService.findByUser(user);
        tokens.forEach(t->tokenService.delete(t));
        Token newToken = new Token(expiredDays);
        newToken.setUser(user);
        tokenService.save(newToken);
        LOGGER.info(user, InfoMessage.TOKEN_OBTAINED + newToken.getToken());
        return newToken;
    }

    public Long authenticateWithToken(String token) throws NotAuthorizedException {
        Long userId = null;
        Token existingToken = tokenService.findByToken(token);
        if(existingToken==null) {
            LOGGER.error(ErrorMessage.INVALID_TOKEN + token);
            throw new NotAuthorizedException(ErrorMessage.NOT_AUTHENTICATED);
        }
        else if(existingToken.isExpired()) {
            throw new NotAuthorizedException(ErrorMessage.SESSION_EXPIRED);
        }
        LOGGER.info(InfoMessage.SUCCESFULLY_AUTH_TOKEN);
        return existingToken.getUser().getId();
    }

}
